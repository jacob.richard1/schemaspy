import re
with open("routines.html", "r") as f:
    lines = f.readlines()
# take out the routines lines like
#                                 <tbody>
# ...
#                                 </tbody>
isRoutineLines = False
with open("routines.html", "w") as f:
    for line in lines:
        if "tbody" in line:
            isRoutineLines = not isRoutineLines
        if not isRoutineLines:
            f.write(line)
with open("indio.public.xml", "r") as f:
    lines = f.readlines()
isRoutineLines = False
with open("indio.public.xml", "w") as f:
    for line in lines:
        if "<routines" in line:
            isRoutineLines = True
        if not isRoutineLines:
            f.write(line)
        if "</routines" in line:
            isRoutineLines = False
# update the count of routines to 0
with open("index.html", "r") as f:
    lines = f.readlines()
isRoutineNext=False
with open("index.html", "w") as f:
    for line in lines:
        if isRoutineNext:
            f.write(re.sub(r"\d+", "0", line))
        else:
            f.write(line)
        isRoutineNext = "Routines" in line
