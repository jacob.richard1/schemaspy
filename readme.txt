•to start the program run
bash main.sh

•to see html output open index.html.

https://schemaspy.sourceforge.net/
descriptions of files:
•main.sh: start the program (bash main.sh). connects to database through docker (put db passwords/connection info and table names in this file)
•overrideCSS.css: override the colors of the css, making everything the same teal color and updating overflowing text
•indio_schema.xml: describe the indio schema (columns and tables)
•deleteroutines.py: the indio database doesnt have sql routines so deleted it from ui (its like sql trigger functions shouldnt be in ui).
