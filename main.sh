# https://schemaspy.sourceforge.net/
# db connection/pass from indio_release.
passdb=passdb # in 1password
hostdb=hostdb # <release_db>.us-east-1.rds.amazonaws.com

# use datalake tables. indio_schema.xml has descriptions of the tables/columns
docker run -v "$PWD:/output" -v "$(pwd)"/indio_schema.xml:/indio_schema.xml:ro schemaspy/schemaspy@sha256:27750fa885a23250742490f601b126f9630a26255754720ca67a076064896845 -t pgsql11 \
-db indio -i \
"(activity_activity)|(authentication_user)|(brokerages_brokerage)|(brokerages_brokerage_forms)|(brokerages_brokeragesettings)|(brokerages_language)|(brokers_broker)|(client_entities_cliententity)|(clients_client)|(clients_clientprofile)|(entities_entity)|(entities_entityformfield)|(form_builder_baseform)|(form_builder_form)|(form_builder_form_lines_of_business)|(form_builder_formblock)|(form_builder_formblockchild)|(form_builder_formfield)|(form_builder_formpage)|(pde_form_builder_pdeform)|(pde_form_builder_pdeform_insurance_types)|(pde_datacontext)|(pde_form_builder_pdeformfield)|(policies_category)|(policies_insurancetype)|(policies_lineofbusiness)|(prefill_prefillform)|(prefill_prefillform_hidden_fields)|(prefill_prefillresponse)|(responses_response)|(responses_responserow)|(responses_responserow_form_blocks)|(responses_responsevalue)|(submissions_formbundle)|(submissions_submission)|(submissions_submission_response_rows)|(submissions_submissionform)|(submissions_submissionformresponserow)" \
-host \
$hostdb -port 5432 -s \
public -u indio_readonly -p $passdb -norows -meta indio_schema.xml -noimplied
# delete the routine functions. Trigger functions is for internal use, irrelevant to pde datalake.
python3 deleteroutines.py
# make the front page all 1 color (dark blue)
mv overrideCSS.css schemaSpy.css
# start the site
open index.html
